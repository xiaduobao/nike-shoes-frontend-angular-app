## Nike Interview Frontend Code Challenge [Angular]

_The provided code document should contain more details._

Please ensure that your backend server is running (on port 8081) before running this app.

#### Development server

Please run `npm install` to get ahead of unforeseen dependency failures.
Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Code scaffolding (if angular cli is installed)

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.



### changes & code improvements.

1. add test cases for method getPrice and getState, Most of lines and contions are covered
2. fix the bug for AppComponent test,  add the imports HttpClientTestingModule
3. fix the test error for 'should render content'


### If you more time, what would you add further.

1. firstly, will consider the getState logic move to the backend as it has some data security risk, then, it will just need a state to Front, no need to expose the price range data.
2. secondly, I would reconstruct the code modules to  make it more clean, follow the rule of Low cohesion and high coupling, not let all files in a folder.

### What were your doubts, and what were your assumptions for the projects?

1. not sure whether need to add the front page for the new API of discount shoe price.

# Any other notes, that are relevant to your task.
1. see the note in backend service's readme.





