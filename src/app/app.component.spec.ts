import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [ HttpClientTestingModule ]

    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'nike-shoes-frontend-angular-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('nike-shoes-frontend-angular-app');
  });

  it('should render content', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content')?.textContent).toContain('Nike Air Max 95 SE');
  });

  it('call getPrice with ShoeId', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const price = app.getPrice('1')
    
    expect(price).toEqual("0");
  });

  it('call getPrice with wrong ShoeId', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const price = app.getPrice('-1')
    expect(price).toEqual("Not Found!");
  });


  it('call getState with wrong ShoeId', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const price = app.getState('-1')
    expect(price).toEqual("Not Found!");
  });


  it('call getState by  ShoeId with different price', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    spyOn(app, "getPrice").and.returnValues("5", "130", "200");

    var status = app.getState('1')
    expect(status).toEqual("Best time to buy!");
    status = app.getState('1')
    expect(status).toEqual("Moderate state, can buy now!");
    status = app.getState('1')
    expect(status).toEqual("Can wait for discount");
  });
});
